import { useState } from 'react';
import { StoreType } from '../types';

export const createStore = (): StoreType => {
  const [innerStore, setInnerStore] = useState<Record<string, any>>({});

  return {
    getState: () => innerStore,
    getSwearValue: (key: string) => innerStore[key],
    setSwearValue: (key: string, payload: any) => setInnerStore(prev => ({ ...prev, [key]: payload })),
  };
};
