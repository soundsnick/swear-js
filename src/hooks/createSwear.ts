import { ActionType, SwearType } from '../types';

export const createSwear = <T>(
  name: string,
  defaultValue: T,
  actions: Record<
    string,
    ActionType<T>>
): SwearType<T> => {
  return [name, defaultValue, actions];
}

