import { ReducerType, SwearType } from '../types';
import { useContext, useEffect, useState } from 'react';
import { StoreContext } from '../store';
import { createAction } from './createAction';

export const useSwear = <T>([name, defaultState, actions]: SwearType<T>): [T, Record<string, ReducerType>] => {
  const store = useContext(StoreContext);
  const [swearValue, setSwearValue] = useState(defaultState);

  useEffect(() => {
    store?.setSwearValue(name, defaultState);
  }, []);

  useEffect(() => {
    if (store) {
      setSwearValue(store.getSwearValue(name));
    }
  }, [store]);

  if (store) {
    const wrappedActions = Object.entries(actions).reduce((acc, [actionName, reducer]) => {
      return {
        ...acc,
        [actionName]: createAction<T>(name, reducer, store),
      };
    }, {});
    return [swearValue, wrappedActions];
  }

  return [swearValue, {}];
};
