import { ActionType, MutateType, StoreType } from '../types';
import { useEffect, useState } from 'react';

export const createAction = <T>(name: string, action: ActionType<T>, store: StoreType) => {
  const [prev, setPrev] = useState(store.getSwearValue(name));

  useEffect(() => {
    setPrev(store.getSwearValue(name));
  }, [store]);
  const mutator = (value: MutateType<T>) => {
    store.setSwearValue(name, value instanceof Function ? value(prev) : value);
  }
  return action(mutator);
};
