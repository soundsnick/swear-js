export type PromiseResolve = (value: unknown) => void;
export type PromiseReject =  (reason?: any) => void;
