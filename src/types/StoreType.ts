export type StoreType = {
  getState: () => Record<string, any>;
  getSwearValue: (key: string) => any;
  setSwearValue: (key: string, payload: any) => void;
}
